//This program deletes an element from a sorted array.

#include<stdio.h>
#include "Binary_Search.c"


int deleteElement(int arr[], int n, int key)
{
    // Find position of element to be deleted
    int pos = binary_search(arr, 0, n, key);
 
    if (pos==-1)
    {
        printf("Element not found");
        return n;
    }
 
    // Deleting element
    int i;
    for (i=pos; i<n; i++)
        arr[i] = arr[i+1];
 
    return n-1;
}


int main()
{
    int i;
    int arr[] = {10, 20, 30, 40, 50};
 
    int n = sizeof(arr)/sizeof(arr[0]);
    int key = 12;
 
    printf("Array before deletion\n");
    for (i=0; i<n; i++)
      printf("%d  ", arr[i]);
 
    n = deleteElement(arr, n, key);
 
    printf("\n\nArray after deletion\n");
    for (i=0; i<n; i++)
      printf("%d  ", arr[i]);
}
