//Inserts an element into a sorted array


#include<stdio.h>

int insert_arr(int arr[], int n, int capacity, int key){
	int i;
	if(n >= capacity)
		return n;
	for(i=n-1 ; (i>=0 && arr[i] > key) ; i--)
		arr[i+1] = arr[i];
	arr[i+1] = key;
	return n+1;
}

int main(){
	int i;
	int arr[10] = {3,4,6,8,12,14};
	int capacity = sizeof(arr)/sizeof(arr[0]);
	int n = 6;
	int key = 1;
	printf("Before Insertion\n");
	for(i=0; i<n; i++)
		printf("%d ", arr[i]);
	printf("\nAfter Insertion\n");
	n = insert_arr(arr, n, capacity, key);
	for(i=0; i<n; i++)
		printf("%d ", arr[i]);
	printf("\n");
	return 0;
}
