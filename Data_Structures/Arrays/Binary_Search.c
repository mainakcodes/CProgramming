#include<stdio.h>

int binary_search(int arr[], int low, int high, int key){
	int mid;
	if(low>high)
		return -1;
	mid = (low + high) / 2;
	if( key == arr[mid])
		return mid;
	else if( key < arr[mid])
		return binary_search(arr, low, mid-1, key);
	else
		return binary_search(arr, mid+1, high, key);
}
/*
int main(){
	int i;
	int arr[] = {5, 6, 8, 9, 12, 13};
	int size = sizeof(arr) / sizeof(arr[0]);   
//size contains 6 in this case....will contain the capacity of the array can be more than the number of elements provided
	
	int key = 1;
	//printf("The position of %d is = %d\n", key, binary_search(arr, 0, size, key)+1);

	return 0;
}
*/
