//This program finds and deletes a key from an unsorted array

#include<stdio.h>



//find function below will find the position of the key in the array
int find(int arr[], int n, int key){
	int i, pos=-1;

        for(i=0; i<n; i++){
                if(arr[i] == key)
		pos = i+1;
        }
	return pos;	
}

//delete function will delete the key from the array
int delete(int arr[], int n, int key){
	int i, pos;
	pos = find(arr,n,key);
	//if element is not found
	if(pos == -1)
		return n;
	//else just shift all the elements one spot to the left starting at pos
	else{
		for(i = pos-1; i<n-1; i++)
			arr[i] = arr[i+1];
		arr[i] = 0;
	}
	return n-1;
}

int main(){
	
	int i, key;
	int arr[] = {100, 20, 130, -4, 23};
	int n = sizeof(arr)/sizeof(arr[0]);
	printf("Array before deletion\n");

	for(i=0; i<n; i++)
		printf("%d ", arr[i]);

	printf("\nEnter the element that you wish to remove: \n");
	scanf("%d", &key);

	n = delete(arr, n, key);
	printf("Array after deletion\n");

	for(i=0; i<n; i++)
		printf("%d ", arr[i]);
	printf("\n");
	return 0;
}
